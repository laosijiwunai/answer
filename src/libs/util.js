import axios from 'axios';
import env from '../../build/env'
import { Confirm } from 'vue-ydui/dist/lib.rem/dialog';

const ajaxUrl = env === 'development' ?
  'http://cpadmin.com:8888/answer' :
  env === 'production' ?
  'http://asmt.jueshengedu.com/answer' :
  'http://asmt.jueshengedu.com/answer'

let util = {
  ajaxUrl: ajaxUrl
};

util.ajax = axios.create({
  baseURL: ajaxUrl,
  timeout: 30000
})

util.ajax.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 添加响应拦截器
util.ajax.interceptors.response.use(function (response) {
  let data = response.data
  if (env === 'development') {
    console.log(`axios respone data:${response.config.url}`, data)
  }
  //判断是否登录
  // if (data.code == 202 || data.code == 401) {
  //   alert(data.msg);
  // } else {
  //   return data
  // }
  return data
}, function (error) {
  return Promise.reject(error)
})
export default util;
