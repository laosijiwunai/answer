import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PcSurvey',
      component: resolve => { require(['@/views/pc/survey.vue'], resolve); }
    },
    {
      path: '/answer',
      name: 'PcAnswer',
      component: resolve => { require(['@/views/pc/answer.vue'], resolve); }
    },
    {
      path: '/score',
      name: 'PcScore',
      component: resolve => { require(['@/views/pc/score.vue'], resolve); }
    },
    {
      path: '/',
      name: 'MSurvey',
      component: resolve => { require(['@/views/mobile/survey.vue'], resolve); }
    },
    {
      path: '/Manswer',
      name: 'MAnswer',
      component: resolve => { require(['@/views/mobile/answer.vue'], resolve); }
    },
    {
      path: '/Mscore',
      name: 'MScore',
      component: resolve => { require(['@/views/mobile/score.vue'], resolve); }
    }
  ]
})
