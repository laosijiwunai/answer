// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import YDUI from 'vue-ydui'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import Vant from 'vant'
import 'vant/lib/vant-css/index.css'
import echarts from 'echarts'

Vue.prototype.$echarts = echarts
Vue.use(Vant);
Vue.use(YDUI);
Vue.use(iView);

Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
